-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-06-2018 a las 01:06:05
-- Versión del servidor: 10.1.33-MariaDB
-- Versión de PHP: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cng2018`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `participante`
--

CREATE TABLE `participante` (
  `id` int(11) NOT NULL,
  `nombre_icat` varchar(255) NOT NULL,
  `receta` varchar(255) NOT NULL,
  `categoria` varchar(255) NOT NULL,
  `decoracion` text NOT NULL,
  `pro_platillo` text NOT NULL,
  `pro_guar` text NOT NULL,
  `pro_empla` text NOT NULL,
  `nombre_p1` varchar(255) NOT NULL,
  `tel_p1` int(15) NOT NULL,
  `email_p1` varchar(255) NOT NULL,
  `nombre_p2` varchar(255) NOT NULL,
  `tel_p2` int(15) NOT NULL,
  `email_p2` varchar(255) NOT NULL,
  `nombre_p3` varchar(255) NOT NULL,
  `tel_p3` int(15) NOT NULL,
  `email_p3` varchar(255) NOT NULL,
  `nombre_inst` varchar(255) NOT NULL,
  `tel_inst` int(15) NOT NULL,
  `email_inst` varchar(255) NOT NULL,
  `nombre_dir1` varchar(255) NOT NULL,
  `cargo_dir1` varchar(255) NOT NULL,
  `tel_dir1` int(15) NOT NULL,
  `email_dir1` varchar(255) NOT NULL,
  `nombre_dir2` varchar(255) NOT NULL,
  `cargo_dir2` varchar(255) NOT NULL,
  `tel_dir2` int(15) NOT NULL,
  `email_dir2` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `participante`
--
ALTER TABLE `participante`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `participante`
--
ALTER TABLE `participante`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
